Scriptname GotE:GotE_MaintenanceScript extends Quest
{This is the central maintenance script for Gear of the Enclave, in case any changes have to be reverted via script. Keeps track of versions, manages integration with AR-1-1-2, and only acts when needed.}

; The following vars are to do with detecting whether a version upgrade has happened.
GlobalVariable Property AR_GotE_Version Auto Const
Float CurrentStoredVersion = 0.0
Bool HasPerformedFirstLoad = false

; The following vars are to do with AR Version 1.1.0 integration.
GlobalVariable Property AR_AmericaRisingTheOilRigUpdateInstalled Auto Const
ObjectReference AR_QuartermasterVendorChest = none
LeveledItem AREnclaveRegularSoldier_Weapons = none
LeveledItem AREnclaveRegularSoldier_Armor = none

; Leveled lists to add to Enclave vendors
LeveledItem Property AR_GearOfTheEnclaveLL_Armors Auto Const
LeveledItem Property AR_GearOfTheEnclaveLL_Weapons Auto Const
LeveledItem Property AR_GearOfTheEnclaveLL_Misc Auto Const
; Leveled lists to add to Enclave NPCs
LeveledItem Property AR_GearOfTheEnclaveLL_ARSoldier_Weapons Auto Const
; Weapons
LeveledItem Property AR_LL_EnclaveAER_9A1Gun_Rifle_SemiAuto Auto Const
LeveledItem Property AR_LL_EnclaveAER_9A1Gun_Rifle_Auto Auto Const
LeveledItem Property AR_LL_EnclaveAER_9A1Gun_SniperRifle Auto Const

LeveledItem Property AR_LL_EnclavePlasmaGun_Rifle_SemiAuto Auto Const
LeveledItem Property AR_LL_EnclavePlasmaGun_Rifle_Auto Auto Const
LeveledItem Property AR_LL_EnclavePlasmaGun_Shotgun_Rifle_SemiAuto Auto Const
LeveledItem Property AR_LL_EnclavePlasmaGun_SniperRifle_SemiAuto Auto Const

LeveledItem Property AR_GearOfTheEnclaveLL_ARSoldier_Armors Auto Const

Event OnInit()
	If HasPerformedFirstLoad == false ; Has an OnLoad on this script ever run before? If not, force the RunMaintenance() function once.
		RunMaintenance()
		HasPerformedFirstLoad = true
	endif
EndEvent

Event Actor.OnPlayerLoadGame(Actor akSender)
	RunMaintenance()
EndEvent

Function RunMaintenance()
    if AR_GotE_Version.GetValue() != CurrentStoredVersion && HasPerformedFirstLoad == true; A new version has loaded, run all maintenance scripts.
    	debug.trace( self + " has detected a version update. Previous version was " + CurrentStoredVersion + " and new version is " + AR_GotE_Version.GetValue())
    	If CurrentStoredVersion < 104 
    	; These versions incorrectly altered the armor leveled list for AR soldiers. Revert this change.
    		debug.trace( self + "Reverting LeveledList additions for America Rising soldier armors.")
    		AREnclaveRegularSoldier_Armor.Revert()
    	endif

    	If CurrentStoredVersion < 105 
    	; Prior to this version, there wasn't a global to track wether A Tale of the Enclave was installed. Grab the correct status now.
    	; Also, AR 1-1-2 integration didn't reflect the latest preferred setup. Correct that here.
    		debug.trace( self + "Setting GlobalVariable AR_AmericaRisingTheOilRigUpdateInstalled.")
    		if Game.IsPluginInstalled("America Rising - A Tale of the Enclave.esp")
    			AR_AmericaRisingTheOilRigUpdateInstalled.SetValue(1.0)

    			debug.trace( self + "Updating AR 1-1-2 integration.")
    			AddEquipmentToAR(105)
    		else
    			AR_AmericaRisingTheOilRigUpdateInstalled.SetValue(0.0)
    		endif
    	endif

    	CurrentStoredVersion = AR_GotE_Version.GetValue()

    elseif HasPerformedFirstLoad == false ; This is the first load, so store the current version.
    	debug.trace( self + "This is GotE's first setup, store the current version.")
    	CurrentStoredVersion = AR_GotE_Version.GetValue()
    elseif AR_GotE_Version.GetValue() == CurrentStoredVersion
    	debug.trace( self + "No GotE upgrade detected. Pausing maintenance script.")
    endif

    CheckForATaleOfTheEnclave()

    RegisterForRemoteEvent(Game.GetPlayer(), "OnPlayerLoadGame")
EndFunction

Function CheckForATaleOfTheEnclave()
	; Here, we'll check to see if the player has America Rising - A Tale of the Enclave version 1.1.0 or higher (but NOT 2.0.0), and if so, adds the items for sale to Quartermaster Barratt's armory and to Enclave NPCs.
	if Game.IsPluginInstalled("America Rising - A Tale of the Enclave.esp") && AR_AmericaRisingTheOilRigUpdateInstalled.GetValue() == 0.0
		debug.trace( self + "GotE Integration: America Rising 1-1-2 detected, but not registered as integrated. Starting integration.")
		AR_AmericaRisingTheOilRigUpdateInstalled.SetValue(1.0) ; Set to true.
		AddEquipmentToAR()
	elseif !Game.IsPluginInstalled("America Rising - A Tale of the Enclave.esp")
		debug.trace( self + "GotE Integration: America Rising 1-1-2 not detected. Disabling integration.")
		AR_AmericaRisingTheOilRigUpdateInstalled.SetValue(0.0) ; Set to false.

		AR_QuartermasterVendorChest = none
		AREnclaveRegularSoldier_Weapons = none
		AREnclaveRegularSoldier_Armor = none
	elseif Game.IsPluginInstalled("America Rising - A Tale of the Enclave.esp") && AR_AmericaRisingTheOilRigUpdateInstalled.GetValue() == 1.0
		debug.trace( self + "GotE Integration: America Rising 1-1-2 detected, and registered as integrated. No action taken.")
	endif
EndFunction

Function AddToARQuartermasterVendorLists(LeveledItem itemToAdd, int Level = 1, int Quantity = 1)
	; Quartermaster's vendor chest
	if AR_QuartermasterVendorChest != none
		AR_QuartermasterVendorChest.AddItem(itemToAdd as Form, Quantity)
	else
		debug.trace( self + "AR_QuartermasterVendorChest = none, could not add " + itemToAdd)
	endif
EndFunction

Function AddToARSoldierArmorLists(LeveledItem itemToAdd, int Level = 1, int Quantity = 1)
	; Enclave Soldier Armor
	if AREnclaveRegularSoldier_Armor != none
		AREnclaveRegularSoldier_Armor.AddForm(itemToAdd as Form, Level, Quantity)
	else
		debug.trace( self + "AREnclaveRegularSoldier_Armor = none, could not add " + itemToAdd)
	endif
EndFunction

Function AddToARSoldierWeaponLists(LeveledItem itemToAdd, int Level = 1, int Quantity = 1)
	; Enclave Soldier Weapons
	if AREnclaveRegularSoldier_Weapons != none
		AREnclaveRegularSoldier_Weapons.AddForm(itemToAdd as Form, Level, Quantity)
	else
		debug.trace( self + "AREnclaveRegularSoldier_Weapons = none, could not add " + itemToAdd)
	endif
EndFunction

Function AddEquipmentToAR(int versionStatus = 0)
	if AR_QuartermasterVendorChest == none
		AR_QuartermasterVendorChest = Game.GetFormFromFile(0x05E708, "America Rising - A Tale of the Enclave.esp") as ObjectReference
	endif
	if AREnclaveRegularSoldier_Weapons == none
		AREnclaveRegularSoldier_Weapons = Game.GetFormFromFile(0x056381, "America Rising - A Tale of the Enclave.esp") as LeveledItem
	endif
	if AREnclaveRegularSoldier_Armor == none
		AREnclaveRegularSoldier_Armor = Game.GetFormFromFile(0x056398, "America Rising - A Tale of the Enclave.esp") as LeveledItem
	endif

	if versionStatus == 0 ; Run the function normally.
		debug.trace( self + "Adding GotE items to AR 1-1-2 as normal.")
		AddToARQuartermasterVendorLists(AR_GearOfTheEnclaveLL_Armors, 1, 4)
		AddToARQuartermasterVendorLists(AR_GearOfTheEnclaveLL_Weapons, 1, 4)
		AddToARQuartermasterVendorLists(AR_GearOfTheEnclaveLL_Misc, 1, 4)

		AddToARSoldierWeaponLists(AR_LL_EnclaveAER_9A1Gun_Rifle_SemiAuto, 1, 1)
		AddToARSoldierWeaponLists(AR_LL_EnclaveAER_9A1Gun_Rifle_Auto, 1, 1)
		AddToARSoldierWeaponLists(AR_LL_EnclavePlasmaGun_SniperRifle_SemiAuto, 1, 1)

		AddToARSoldierWeaponLists(AR_LL_EnclavePlasmaGun_Rifle_SemiAuto, 17, 1)
		AddToARSoldierWeaponLists(AR_LL_EnclavePlasmaGun_Rifle_Auto, 17, 1)
		AddToARSoldierWeaponLists(AR_LL_EnclavePlasmaGun_SniperRifle_SemiAuto, 17, 1)
		AddToARSoldierWeaponLists(AR_LL_EnclavePlasmaGun_Shotgun_Rifle_SemiAuto, 17, 1)

		AddToARSoldierArmorLists(AR_GearOfTheEnclaveLL_ARSoldier_Armors, 1, 1)
	endif

	if versionStatus == 105 ; Run this configured for Version 1.0.5's maintenance script.
		debug.trace( self + "Adding GotE items to AR 1-1-2 as part of updating to version 1.0.5 from a previous version.")
		AddToARQuartermasterVendorLists(AR_GearOfTheEnclaveLL_Armors, 1, 3)
		AddToARQuartermasterVendorLists(AR_GearOfTheEnclaveLL_Weapons, 1, 3)
		AddToARQuartermasterVendorLists(AR_GearOfTheEnclaveLL_Misc, 1, 3)

		AddToARSoldierWeaponLists(AR_LL_EnclaveAER_9A1Gun_Rifle_SemiAuto, 1, 1)
		AddToARSoldierWeaponLists(AR_LL_EnclaveAER_9A1Gun_Rifle_Auto, 1, 1)
		AddToARSoldierWeaponLists(AR_LL_EnclavePlasmaGun_SniperRifle_SemiAuto, 1, 1)

		AddToARSoldierWeaponLists(AR_LL_EnclavePlasmaGun_Rifle_SemiAuto, 17, 1)
		AddToARSoldierWeaponLists(AR_LL_EnclavePlasmaGun_Rifle_Auto, 17, 1)
		AddToARSoldierWeaponLists(AR_LL_EnclavePlasmaGun_SniperRifle_SemiAuto, 17, 1)
		AddToARSoldierWeaponLists(AR_LL_EnclavePlasmaGun_Shotgun_Rifle_SemiAuto, 17, 1)

		AddToARSoldierArmorLists(AR_GearOfTheEnclaveLL_ARSoldier_Armors, 1, 1)
	endif
EndFunction