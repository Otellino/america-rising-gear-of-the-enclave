;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname GotE:Fragments:Quests:QF_AR_GotE_ItemDistributionQ_01004D5D Extends Quest Hidden Const

;BEGIN FRAGMENT Fragment_Stage_0000_Item_00
Function Fragment_Stage_0000_Item_00()
;BEGIN AUTOCAST TYPE GotE:GotE_ItemDistributionQuest
Quest __temp = self as Quest
GotE:GotE_ItemDistributionQuest kmyQuest = __temp as GotE:GotE_ItemDistributionQuest
;END AUTOCAST
;BEGIN CODE
; Add items to vendors
kmyQuest.AddToVendorArmorLists(pAR_GearOfTheEnclaveLL_Armors, 1, 4)
kmyQuest.AddToVendorWeaponLists(pAR_GearOfTheEnclaveLL_Weapons, 1, 4)
kmyQuest.AddToVenderGenericLists(pAR_GearOfTheEnclaveLL_Misc, 1, 4)
; Shut down
setStage(1000)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_1000_Item_00
Function Fragment_Stage_1000_Item_00()
;BEGIN AUTOCAST TYPE GotE:GotE_ItemDistributionQuest
Quest __temp = self as Quest
GotE:GotE_ItemDistributionQuest kmyQuest = __temp as GotE:GotE_ItemDistributionQuest
;END AUTOCAST
;BEGIN CODE
stop()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

LeveledItem Property pAR_GearOfTheEnclaveLL_Armors Auto Const Mandatory

LeveledItem Property pAR_GearOfTheEnclaveLL_Weapons Auto Const Mandatory

LeveledItem Property pAR_GearOfTheEnclaveLL_Misc Auto Const Mandatory
